import { useState, useRef, useMemo, useEffect } from 'react';
import './App.css';


function debounce(callBackFn, delay = 1000) {
  let timeoutId = null;
  return (...args) => {
    console.log({ clearTimeout });
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      callBackFn(...args);
    }, delay);
  }
}


function App() {
  const [items, setItems] = useState(['one', 'two', 'three', 'four', 'five']);
  const [query, setQuery] = useState("");
  const inputRef = useRef();


  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/todos')
    .then((resp) => resp.json())
    .then((data) => {
      setItems(data.map(todo => todo.title));
    });
  }, []);


  const debouncedSetQuery = debounce((text) => setQuery(text));


  function onSubmit(ev) {
    ev.preventDefault();
    const value = inputRef.current.value;
    if (value === "") return;
    setItems(prev => {
      return [...prev, value];
    });
    inputRef.current.value = "";
  }

  function onSearch(ev) {
    ev.preventDefault();
    debouncedSetQuery(ev.target.value);
  }

  console.log('cmp render...', {query});

  const filteredItems = useMemo(() => {
    return items.filter((item) => {
      return item.toLowerCase().includes(query.toLowerCase());
    });
  }, [items, query]);

  return <>
    Search 
    <input onChange={onSearch} type="search" />
    { /* <button onClick={onSearch}>search</button> */ }
    <br />
    <br />
    <form onSubmit={onSubmit}>
      New Item: <input ref={inputRef} type="text" />
      <button type="submit">Add</button>
    </form>
    <h3> Items: </h3>
    {
      filteredItems.map((item, index) => (
        <div key={index}>
          {item}
        </div>
      ))
    }
  </>
}

export default App;
